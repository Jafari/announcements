## Mark the status of this text in the following line.
## * Use "open-for-edit" if it's too early for anyone to translate anything
## * Use "content-frozen" if the content has been stabilized, but is being
##   reviewd by native speakers;  translators can start their work now
## * Use "sent" if the issue has been sent our; furhter changes should now
##   be done in Debian's webwml repository
# Status: [open-for-edit]
# $Id$
# $Rev$

##
## Template for upcoming announcements;  please copy it to the YYYY folder, and
## try to name it YYYY-MM-DD-whats-it-about.wml ; that helps a lot keeping
## an overwiew
##

<define-tag pagetitle>Debian Project mourns the loss of Ian
Murdock</define-tag>

<define-tag release_date>2016-01-03</define-tag>
#use wml::debian::news

# Some things that may be needed to get done so this draft becomes
# a nice announcement:
# - maybe some rewording or more cutting would improve it
# (the "releases" quotes?) -> partly done
# - Maybe add some more lines about Ian's work after Debian
# ^-> somehow done
# - mention+link to the Docker statement?
# A line about the websites mourning logo -> somehow done
# - Comments, rewording and improvements, in general
# - More "human touch"

##
## Translators should uncomment the following line and add their name
## Leaving translation at 1.1 is okay; that's the first version which will
## be added to Debian's webwml repository
##
# ← this one must be removed; not that one → #use wml::debian::translation-check translation="1.1" maintainer=""


## You may use plain HTML here; we recommend to stick with paragraphs,
## lists and tables
<p>
The Debian Project sadly announces that it has lost the founder of its
community and project, Ian Murdock.
</p>

<p>Debian is only a part of Ian's legacy but perhaps the one that he is most 
known for.</p>

<p>Ian was introduced to computers early in his life and his curiousity 
turned to familiarity which led him to start actively programming at 9 years of 
age. Later as a young adult at the Krannert School of Management a 
mandatory 
programming class rekindled his fascination with computer programing along with 
an idea and an opportunity to make something better.</p>

<p>
Ian started the Debian Project in August of 1993, releasing the first
versions of Debian later that same year. At that time, the whole concept of a 
<q>distribution</q> of Linux was new. Inspired as he says with Linus Torvalds 
own sharing of Linux, he released what he had intended, Debian, to be a 
distribution which would be made openly, in the spirit of Linux and GNU.  
</p>

<p>
With that simple gesture Ian started a movement in the world 
of software, many developers joined him in this task of creating better 
software in a better world. 
</p>

<p> From his <a 
href="https://www.debian.org/doc/manuals/project-history/ap-manifesto.en
.html">Debian Mainfesto</a>:
  
# begin-paragraph copied from
#
#https://www.debian.org/doc/manuals/project-history/ap-manifesto.en.html
#

<q>The Debian design process is open to ensure that the system is of the
highest quality and that it reflects the needs of the user community. By
involving others with a wide range of abilities and backgrounds, Debian
is able to be developed in a modular fashion. [...]
Involving others also ensures that
valuable suggestions for improvement can be incorporated into the
distribution during its development; thus, a distribution is created
based on the needs and wants of the users rather than the needs and
wants of the constructor.</q>

# end-paragraph
</p>

<p>
His sharp focus was on creating a distribution and community culture
that did the right thing, be it ethically, or technically.
</p>

<p>
Releases went out when they were ready, and the project's
staunch stance on Software Freedom were and are still the gold standard
in the Free and Open Source world.
</p>

<p>
#begin-paragraph copied from
# https://www.debian.org/doc/manuals/project-history/ch-detailed.en.html

Debian 0.01 through Debian 0.90 were released between August and
December of 1993. Ian Murdock writes:
</p>

<p>
<q>Debian 0.91 was released in January 1994. It had a primitive package
system [...]. By this time, there were a few dozen people working on Debian, 
though I was still mostly putting together the releases myself. 0.91 was the 
last release done in this way.</q>
</p>

<p>
<q>Most of 1994 was spent organizing the Debian Project so that others
could more effectively contribute, as well as working on dpkg [...].</q>
</p>

<p>
<q>Debian 0.93 Release 5 happened in March 1995 and was the first
"modern" release of Debian: there were many more developers by then (though I
can't remember exactly how many), each maintaining their own packages,
and dpkg was being used to install and maintain all these packages after
a base system was installed.</q>
</p>

<p>
<q>Debian 0.93 Release 6 happened in November 1995 and was the last
a.out
release. There were about sixty developers maintaining packages in
0.93R6. If I remember correctly, dselect first appeared in 0.93R6.</q>
</p>

<p>
Ian Murdock also notes that Debian 0.93R6 <q>... has always been my
favorite release of Debian</q>, although he admits to the possibility of
some personal bias, as he stopped actively working on the project in
March 1996.
</p>

#end-paragraph

#begin-paragraph
<p>
Ian Murdock led Debian until March 1996, when he appointed Bruce
Perens as the next leader of the project.
</p>


<p>
The devotion to the right thing guided Ian's work, both in Debian and
in the subsequent years, always working towards
the best possible future.
</p>

<p>
Debian would go on to become the world's Universal Operating System, found on 
everything from the smallest embedded devices to the largest cluster 
systems, to the Space Station because "of course it runs Debian" which has been
ported across multiple architectures and hardware. It truly is a universal 
operating system. 
</p>

<p>
Ian's dream lives on, Debian is made up of a strong community that 
has fostered development, growth, wonder, and remains incredibly
active with thousands of developers working untold hours to bring
the world a reliable and secure operating system.  Debian has sparked 
an interest in so many users, contributors, developers, and the general 
public then, now, and hopefully far into the future. 
</p>

<p>
From the bottom of our hearts, we thank you Ian.
</p>

<p>Throughout the Debian infrastructure our websites and services mark our
reflection and mourning with a darkend homepage banner and ribbon on our logos.
The thoughts of the Debian Community are with Ian's family in this difficult
time.</p>

<p>
His family has asked for privacy and we very much wish to respect their 
desires.</p>

<p>Within our Debian community and for the Linux community condolences may be sent
to <a href="mailto:in-memoriam-ian@debian.org">in-memoriam-ian@debian.org</a> 
where they will be kept and archived.</p>

<p>This email address will be active until the end of January 2016.
The Debian Project will then provide the archive to the family and publish the 
contents later this year if it is with the wishes of the family.</p>



<h2>About Debian</h2>

<p>The Debian Project is an association of Free Software developers who
volunteer their time and effort in order to produce a completely free
operating system known as Debian.</p>

<h2>Contact Information</h2>

<p>For further information, please visit the Debian web pages at
<a href="$(HOME)/">https://www.debian.org/</a> or send mail to
&lt;press@debian.org&gt;.</p>
