# Status: [open-for-edit]
# $Id: 2012-03-30-OSI.wml 3462 2012-03-27 22:12:46Z moray $
# $Rev: 3462 $

<define-tag pagetitle>El proyecto Debian se une a la OSI</define-tag>

## When is this announcement likely to be send out?  Please keep in mind,
## that it should also be reviewed and translated
<define-tag release_date>2012-03-30</define-tag>
#use wml::debian::news

##
## Translators should uncomment the following line and add their name
## Leaving translation at 1.1 is okay; that's the first version which will
## be added to Debian's webwml repository
##
# #use wml::debian::translation-check translation="1.1" maintainer=""

<p>
El proyecto Debian se complace en anunciar que se ha unido a la
<a href="http://opensource.org/">Open Source Initiative (<q>OSI</q>)</a>
como un proyecto afiliado.
## what is OSI? 

La OSI fue fundado en 1998 por Eric S. Raymond and Bruce Perens, con el
objetivo de explicar, defender y proteger el termino Código Abierto 
<q>Open Source</q>. Debian comparte el deseo de OSI en fomentar el
Software Libre. El contrato social de Debian se compromete a producir
un sistema 100% <a href="$(HOME)/intro/free">libre</a>.
</p>

<p>
La 
<a href="http://opensource.org/docs/osd">definición de Open Source</a>
de la OSI está muy relacionada con las 
<a href="$(HOME)//social_contract#guidelines">Directrices
sobre software libre de Debian</a>, el documento que define los 
tipos de licencia de software que son aceptadas por el proyecto Debian.

Durante muchos años, la OSI ha ayudado a que la marca <q>Open Source</q> 
tenga reconocimiento, particularmente en el mundo corporativo. OSI
decidió recientemente convertirse en una organización dirigida por sus miembros para 
lograr está misión, y como primer paso ha invitado a los proyectos de 
software libre y código abierto <q>Open Source</q>
a que se conviertan en miembros afiliados.
</p>

<p>
Al convertirse en un proyecto afiliado a la OSI, el proyecto Debian reconoce
los esfuerzos de la OSI en alcanzar metas compartidas por ambas organizaciones.
Sin embargo, el proyecto Debian no adoptará automáticamente las decisiones de OSI
sobre la aceptación de licencias de software particulares
y mantendrá un proceso de revisión de licencias independientes.
</p>

<p>
<q>Todavía tenemos que superar muchos desafíos para la adopción 
de software libre a nivel general,</q> dijo Stefano Zacchiroli, Lider del proyecto Debian. 
<q>Mientras Debian es capaz perfectamente de enfrentar desafíos <em>técnicos</em>,
otras organizaciones son más capaces de enfrentar los <em>políticos</em>, como el 
<acronym title="fear, uncertainty, and doubt">FUD</acronym> anti software libre corporativo,
o las leyes injustas que hacen ilegal compartir piezas de software libre.
Valoro el trabajo que la OSI está haciendo en esas áreas. Gracias a su nuevo
esquema de afiliación, espero ver a Debian tomar un rol activo en OSI.</q>
</p>

## a nice quote from OSI people

<p>
Simon Phipps de OSI dio la bienvenida a Debian, diciendo: <q>Es
bueno tener a Debian como nuevo afiliado; la amplia experiencia
del proyecto será una ganancia valiosa. La junta está dispuesta
a hacer progresos y contar con miembros afiliados diversos y representativos
es crucial si vamos a tomar decisiones bien fundadas en cada uno
de los cambios desafiantes que anticipamos.</q>
</p>



<h2>Acerca de la Open Source Initiative</h2>
<p>
La Open Source Initiative (OSI) es la administradora de la 
<a href="http://opensource.org/docs/osd">definición de Código Abierto 
<q>Open Source Definition (OSD)</q></a> y la comunidad reconocida para 
la revisión y aprobación de licencias OSD.
La OSI está participando activamente en la creación de comunidades 
de Código Abierto <q>Open Source</q>, educación y promoción pública
para promover el conocimiento y la importancia del software no privativo.
</p>



<h2>Acerca de Debian</h2>

<p>
El proyecto Debian fue fundado en 1993 por Ian Murdock para ser un proyecto
de la comunidad verdaderamente libre. Desde entonces el proyecto ha crecido
hasta ser uno de los proyectos más grandes e importantes de software libre.
Miles de voluntarios en todo el mundo trabajan juntos para crear y mantener
programas para Debian. Se encuentra traducido en 70 idiomas y soporta una gran
cantidad de arquitecturas de computadoras, por lo que el proyecto se refiere a
si mismo como <q>el sistema operativo universal</q>.
</p>


<h2>Información de contacto</h2>

<p>Para mayor información visite el sitio web de Debian en <a
href="http://www.debian.org/">http://www.debian.org/</a>, o envíe un correo a
&lt;press@debian.org&gt;.</p>
